from ssl import PROTOCOL_TLS_CLIENT
from tkinter import CASCADE
from django.db import models


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField()
    content = models.TextField()
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return (
            +self.name
            + " by "
            + self.author
            + " by "
            + self.description
            + " by "
            + self.image
            + " by "
            + self.created
            + " by "
            + self.updated
        )


class Step(models.Model):
    order = models.SmallIntegerField()
    directions = models.TextField()
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.order + " by " + self.directions + " by " + self.recipe


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name + " by " + self.abbreviation


class FoodItem(models.Model):
    name = models.CharField(max_length=100)

    def str(self):
        return "by " + self.name


class Ingredient(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure", related_name="ingredients", on_delete=models.PROTECT
    )
    food = models.ForeignKey(
        "fooditem", related_name="ingredients", on_delete=models.PROTECT
    )

    def __str__(self):
        return self.recipe + " by " + self.measure + " by " + self.food
