from django.contrib import admin
from recipes.models import FoodItem, Recipe, Step, Measure, Ingredient


class RecipeAdmin(admin.ModelAdmin):
    pass


class StepAdmin(admin.ModelAdmin):
    pass


class MeasureAdmin(admin.ModelAdmin):
    pass


class FootItemAdmin(admin.ModelAdmin):
    pass


class IngredientAdmin(admin.ModelAdmin):
    pass


admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Step, StepAdmin)
admin.site.register(Measure, MeasureAdmin)
admin.site.register(FoodItem, FootItemAdmin)
admin.site.register(Ingredient, IngredientAdmin)
